# gRpc test project

# Technology stack:
- Java 11
- gRPC server
- gRPC client
- protobuf
- http client
- junit 5

# Task:
Implement gRPC server that receives object with adress of web resource(example: google.com) in the request and returns the response code of the web resource and the time during which the
resource responded. Create tests using at least 20 web resources:

- a) Single threaded blocking call;
- b) Single threaded non blocking call;
- c) Multithreaded blocking call;
- d) Multi-thread non-blocking call;

# How to test:
- ```git clone git@gitlab.com:stelionser/health-check.git```
- ```cd health-check```
- ```mvn clean test```

![Test result screenshot](https://i.imgur.com/aZahU3O.png)

