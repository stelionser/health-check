package com.loopme.healthcheck.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.loopme.healthcheck.HealthCheckResponse;
import com.loopme.healthcheck.server.HealthCheckServer;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HealthCheckClientTest {

    private static HealthCheckServer server;
    private static HealthCheckClient client;
    private static List<String> urls;

    @BeforeAll
    public static void setup() throws InterruptedException, IOException {
        int port = 50051;

        server = new HealthCheckServer(port);
        server.start();

        waitingServerStart(port);

        client = new HealthCheckClient("localhost", port);

        urls = Arrays.asList(
            "https://www.google.com/",
            "https://photos.google.com/",
            "https://open.spotify.com/",
            "https://translate.google.com.ua/",
            "https://keep.google.com/",
            "https://www.youtube.com/",
            "http://habr.com/",
            "https://gitlab.com/",
            "https://github.com/",
            "https://refactoring.guru/",
            "https://www.shortn0tes.com/",
            "https://o7planning.org/",
            "https://jhooq.com/",
            "https://kubernetes.io/",
            "https://helm.sh/",
            "https://www.docker.com/",
            "https://www.jetbrains.com/",
            "https://kotlinlang.org/",
            "http://aliexpress.com/",
            "https://mail.google.com/"
        );
    }

    @AfterAll
    public static void exit() throws InterruptedException {
        server.stop();
    }

    private static void waitingServerStart(int port) throws InterruptedException, IOException {
        Thread.sleep(500L);
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress("localhost", port), 5000);
        }
    }

    @Test
    @DisplayName("Single threaded blocking call")
    public void singleThreadedBlockingCall() {
        List<HealthCheckResponse> healthCheckResponses = new ArrayList<>();
        urls.forEach(url -> healthCheckResponses.add(client.callByUrl(url)));

        assertEqualsHealthCheckResponses(healthCheckResponses);
    }

    @Test
    @DisplayName("Single threaded non blocking call")
    public void SingleThreadedNonBlockingCall() throws ExecutionException, InterruptedException {
        List<Future<HealthCheckResponse>> futures = urls.stream()
            .map(client::callByUrlAsync)
            .collect(Collectors.toList());

        List<HealthCheckResponse> healthCheckResponses = new ArrayList<>();
        for (Future<HealthCheckResponse> future : futures) {
            healthCheckResponses.add(future.get());
        }

        assertEqualsHealthCheckResponses(healthCheckResponses);
    }

    @Test
    @DisplayName("Multithreaded blocking call")
    public void multithreadedBlockingCall() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(8);

        List<HealthCheckResponse> healthCheckResponses = new ArrayList<>();
        for (String url : urls) {
            healthCheckResponses.add(executorService.submit(new CallHealthCheckCallable(url)).get());
        }

        assertEqualsHealthCheckResponses(healthCheckResponses);
    }

    @Test
    @DisplayName("Multi-thread non-blocking call")
    public void multiThreadNonBlockingCall() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(8);

        List<Future<HealthCheckResponse>> responseFutures = new ArrayList<>();
        for (String url : urls) {
            responseFutures.add(executorService.submit(new CallHealthCheckCallable(url)));
        }

        List<HealthCheckResponse> healthCheckResponses = new ArrayList<>();

        for (Future<HealthCheckResponse> responseFuture : responseFutures) {
            healthCheckResponses.add(responseFuture.get());
        }

        assertEqualsHealthCheckResponses(healthCheckResponses);
    }

    private static class CallHealthCheckCallable implements Callable<HealthCheckResponse> {

        private final String url;

        public CallHealthCheckCallable(String url) {
            this.url = url;
        }

        public HealthCheckResponse call() {
            return client.callByUrl(url);
        }
    }

    private void assertEqualsHealthCheckResponses(List<HealthCheckResponse> healthCheckResponses) {
        assertFalse(healthCheckResponses.isEmpty());
        assertEquals(urls.size(), healthCheckResponses.size());
        assertArrayEquals(urls.toArray(), healthCheckResponses.stream().map(HealthCheckResponse::getUrl).toArray());
        healthCheckResponses.forEach(response -> {
                assertTrue(response.getTime() > 0);
            }
        );
    }
}