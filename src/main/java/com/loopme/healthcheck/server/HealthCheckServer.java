package com.loopme.healthcheck.server;

import java.io.IOException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import io.grpc.Server;
import io.grpc.ServerBuilder;

public class HealthCheckServer {

    private static final Logger LOG = LogManager.getLogger(HealthCheckServer.class);

    private final Server server;
    private final int port;

    public HealthCheckServer(int port) {
        this.port = port;

        server = ServerBuilder.forPort(port)
            .addService(new HealthCheckImpl())
            .build();
    }

    public void start() {
        LOG.info("Try to start HealthCheckServer on port: " + port);

        try {
            server.start();
        } catch (IOException e) {
            LOG.error("Can't start HealthCheckServer! " + e);
        }

        LOG.info("HealthCheckServer started.");
    }

    public void stop() throws InterruptedException {
        server.shutdown();
        server.awaitTermination();
    }
}
