package com.loopme.healthcheck.server;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.loopme.healthcheck.HealthCheckRequest;
import com.loopme.healthcheck.HealthCheckResponse;
import com.loopme.healthcheck.HealthCheckServiceGrpc;

import example.StatusCodeOuterClass;
import io.grpc.stub.StreamObserver;

public class HealthCheckImpl extends HealthCheckServiceGrpc.HealthCheckServiceImplBase {

    private static final Logger LOG = LogManager.getLogger(HealthCheckImpl.class);

    @Override
    public void healthCheck(HealthCheckRequest request, StreamObserver<HealthCheckResponse> responseObserver) {
        LOG.info("Got request to health check service by url: " + request.getUrl());
        long startExecutionTime = System.currentTimeMillis();

        StatusCodeOuterClass.StatusCode statusCode = getHealthCheckStatusCodeByUrl(request.getUrl());

        double executionTime = System.currentTimeMillis() - startExecutionTime;
        LOG.info("Health check service "
            + "[url: " + request.getUrl() + "] "
            + "execution time: " + executionTime
            + " status code: " + statusCode.getNumber());

        HealthCheckResponse response = HealthCheckResponse.newBuilder()
            .setUrl(request.getUrl())
            .setStatusCode(statusCode)
            .setTime(executionTime)
            .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    private StatusCodeOuterClass.StatusCode getHealthCheckStatusCodeByUrl(String url) {
        HttpClient httpClient = HttpClient.newHttpClient();
        try {
            HttpResponse<String> response = httpClient.send(HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()
                .build(), HttpResponse.BodyHandlers.ofString());

            return StatusCodeOuterClass.StatusCode.forNumber(response.statusCode());
        } catch (IOException | InterruptedException e) {
            LOG.error("Can't call service by url: " + url);
            e.printStackTrace();
            return StatusCodeOuterClass.StatusCode.StatusCode_INTERNAL_SERVER_ERROR;
        }
    }
}
