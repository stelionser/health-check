package com.loopme.healthcheck.client;

import java.util.concurrent.Future;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.loopme.healthcheck.HealthCheckRequest;
import com.loopme.healthcheck.HealthCheckResponse;
import com.loopme.healthcheck.HealthCheckServiceGrpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class HealthCheckClient {

    private static final Logger LOG = LogManager.getLogger(HealthCheckClient.class);

    private final ManagedChannel channel;

    public HealthCheckClient(String address, int port) {
        LOG.info("Try to create HealthCheckClient [address: " + address + "] [port: " + port + "].");
        channel = ManagedChannelBuilder.forAddress(address, port)
            .usePlaintext()
            .build();
        LOG.info("HealthCheckClient was created.");
    }

    public HealthCheckResponse callByUrl(String url) {
        LOG.info("Try to call health check by url: " + url);
        HealthCheckServiceGrpc.HealthCheckServiceBlockingStub client = HealthCheckServiceGrpc.newBlockingStub(channel);
        HealthCheckResponse response = client.healthCheck(HealthCheckRequest.newBuilder().setUrl(url).build());

        LOG.debug("Got response from health check: [url: " + response.getUrl() + "] [time execution " + response.getTime() + "].");
        return response;
    }

    public Future<HealthCheckResponse> callByUrlAsync(String url) {
        LOG.info("Try to call health check async by url: " + url);
        HealthCheckServiceGrpc.HealthCheckServiceFutureStub client = HealthCheckServiceGrpc.newFutureStub(channel);
        Future<HealthCheckResponse> healthCheckResponseFuture =
            client.healthCheck(HealthCheckRequest.newBuilder().setUrl(url).build());

        LOG.debug("Got future response from health check async by url: " + url);
        return healthCheckResponseFuture;
    }
}
